---
supertitle: Report
report: E999R01
title: "How to end the system of animal exploitation: The definitive answer"
subtitle: Practical solutions to overcome speciesism

author:
  - id: 1
    name: Jacob Peacock
    orcid: 0000-0002-4834-8132
    affiliation:
      - The Humane League Labs, Rockville, MD, USA
    email: jpeacock@thehumaneleague.org
    equal: yes           # to denote equal contribution 
    corresponding: yes   # to denote the corresponding author
  - id: 2
    name: Samara Mendez
    orcid: 0000-0002-8179-744X
    affiliation:
      - The Humane League Labs, Rockville, MD, USA
      - University of Colorado Boulder, Boulder, CO, USA
    email: smendez@thehumaneleague.org
    equal: yes           # to denote equal contribution 
  - id: 3
    name: Janosch Linkersdörfer
    orcid: 0000-0002-1577-1233
    affiliation:
      - The Humane League Labs, Rockville, MD, USA
      - Goethe University Frankfurt, Frankfurt am Main, Germany
    email: jlinkersdoerfer@thehumaneleague.org
  - id: 4
    name: Giovana Vieira
    orcid: 0000-0002-3219-0174 
    affiliation:
      - The Humane League Labs, Rockville, MD, USA
    email: gviera@thehumaneleague.org

abstract: |
  This is the executive summary; it summarizes the report. This is the
  executive summary; it summarizes the report.This is the executive
  summary; it summarizes the report. This is the executive summary. It
  should summarize the report. This is the executive summary. It should
  summarize the report. This is the executive summary; it summarizes the
  report. This is the executive summary; it summarizes the report. This
  is the executive summary; it summarizes the report. This is the
  executive summary; it summarizes the report. This is the executive
  summary; it summarizes the report. This is the executive summary. It
  should summarize the report. This is the executive summary. It should
  summarize the report. This is the executive summary; it summarizes the
  report. This is the executive summary; it summarizes the report. This
  is the executive summary; it summarizes the report. This is the
  executive summary; it summarizes the report. This is the executive
  summary; it summarizes the report. This is the executive summary.

bibliography: test.bib
output:
  thll::report_html:
    toc: true                # print table of contents (Default: TRUE)
    toc_depth: 3             # section levels to include (Default: 1; Max: 3)
    number_sections: true    # use section numbers (Default: FALSE)
    # keep_md: yes           # keep .md file (e.g., for trouble shooting)
    # code_folding: hide     # hide code chunks by default (see below)
    
#   ____________________________________________________________________________
#   optional...                                                             ####

# if DOI exists
doi: 10.17605/OSF.IO/Z2GXN

# if applicable, set OSF information

## OSF repository
osf_repo: z2gxn

## OSF badges (set all that apply to `true`)
osf_badges:
  data: true
  materials: true
  preregistered: false
  preregisteredplus: true
  
## Text that is displayed in the OSF box if no badges are selected above
osf_text: All files related to this project are available in the associated

# if applicable, set URL for update button (CANNOT include https://):
updateurl: thehumaneleague.org/labs

# if this is a revision, set the original publication date:
publicationdate: June 12, 2000
---

```{r setup, include=FALSE}
options(width=20)
knitr::opts_chunk$set(echo = TRUE, comment="")
```

# Introduction

This document demonstrates how to use the HTML template for The Humane League (THLL) publications with R Markdown. The HTML template and `.css` file as well as all other needed resources (fonts, images, etc.) are included in---and will automatically be installed with---the associated R package. In the following, we will demonstrate different features of Pandoc and R Markdown. Please compare the `.Rmd` input file with the rendered `.html` output file to see how the Markdown syntax relates to how the document is rendered.

# Sectioning

This is a section. `r stringi::stri_rand_lipsum(1)`

## Subsection

This is a subsection. `r stringi::stri_rand_lipsum(1)`

### Subsubsection

This is a subsubsection. `r stringi::stri_rand_lipsum(1)`

#### Paragraph

This is a paragraph. `r stringi::stri_rand_lipsum(1)`

# (Pandoc) Markdown syntax {#pandoc}

## Basic formatting

Text can be **bold**, *italic*, or `verbatim` (e.g., for code).

This is a quotation:

> *We are, quite literally, gambling with the future of our planet---for the sake of hamburgers.*
>
> --- Peter Singer, Animal Liberation

Explicit URLs and email addresses are automatically converted to clickable links, e.g., https://thehumaneleague.org and jpeacock@thehumaneleague.org. Links can also be shown with alternative text, e.g., The Humane League's [website](https://thehumaneleague.org) and Jacob's [email address](mailto:jpeacock@thehumaneleague.org).

<p style="text-align: center;">One can also use HTML syntax directly if one has or wants to.</p>

## Footnotes

Footnotes can be added like this.^[This is a footnote.]

## Lists

This is an *unordered* list:

- first item
- second item
    + first subitem
    + second subitem
        + first subsubitem
        + second subsubitem
- third item

\noindent This is an *ordered* list:

1. first item
1. second item
1. third item
1. fourth item
1. fifth item
1. sixth item
1. seventh item
1. eighth item
1. ninth item
1. tenth item
    i. first subitem
    i. second subitem
    i. third subitem
    i. fourth subitem
    i. fifth subitem
    i. sixth subitem
    i. seventh subitem
    i. eighth subitem
    i. ninth subitem
    i. tenth subitem
    i. eleventh subitem
        a. first subsubitem
        a. second subsubitem
        a. third subsubitem
1. eleventh item

## Citations

Citations can be added using the following syntax:

- `[@citation1]` / `[@citation2; @citation3]` for parenthetical citations.^[Suffixes can be added to parenthetical citation, e.g., `[@citation1, p. 15]`. Prefixes are ignored in the HTML template.]
- `@citation4` for in-text citations.
- It is not currently possible to cite just the author (like when using the PDF template) as this feature is not implemented in Pandoc yet.^[See https://github.com/jgm/pandoc-citeproc/issues/377.]

Here we demonstrate their usage with different cited materials to show how they will be listed in the reference list. This is a book [@McElreathStatisticalRethinkingBayesian2019, p. 89]; this is a chapter in a book [@Dhontpsychologyspeciesism2020]. These are two journal publications [@PooreReducingfoodenvironmental2018, p. 990; @SpringmannOptionskeepingfood2018].  This is a conference paper [@McKinneyDataStructuresStatistical2010]. This is a thesis [@VentinValuingFarmAnimal2020]. This is a report [@AttwoodPlaybookGuidingDiners2020]. This is an online resource *with* a publication date [@USDAChickensEggs2018], this is one *without* a publication date [@USDAMonthlyUSDACageFree]. This is a website [@ChickenWatch]. This is a software application [@RCoreTeamLanguageEnvironmentStatistical2020]. Poore and Nemecek said one thing [@PooreReducingfoodenvironmental2018], while Springmann et al. said something else [@SpringmannOptionskeepingfood2018, pp. 520-522].

## Mathematical notation

Math is based on LaTeX syntax.^[See, e.g., https://en.wikibooks.org/wiki/LaTeX/Mathematics or https://www.overleaf.com/learn/latex/mathematical_expressions.] To display math inline, one has to enclose mathematical expressions in `$...$`, e.g., $\sum_{n=1}^{10} n^2$. To add an equation, one can utilize LaTeX equation environments:

\begin{equation}
Y_i = \beta_0 + \beta_1 X_i + \epsilon_i
(\#eq:regression)
\end{equation}

# R Markdown syntax/elements

## Code

One can embed arbitrary R code^[A number of other programming languages are also supported, see https://bookdown.org/yihui/rmarkdown/language-engines.html.] anywhere in the document like "the square root of 36 is `r sqrt(36)`". Furthermore, one can use "code chunks" to execute bigger chunks of R code and have the results displayed in the document (or not^[For an overview of different chunk options which determine how code chunks are displayed in the resulting document, see https://yihui.org/knitr/options/.]).

```{r glm-iris}
# select variables
y <- iris$Species
x <- iris$Sepal.Length

# print regression model
glm(y~x, family = 'binomial')
```

## Figures

Code chunks can also be used to embed plots generated with R (or other programming languages). The appearance of figures in the output document can be modified using chunk options.^[See, e.g., http://zevross.com/blog/2017/06/19/tips-and-tricks-for-working-with-images-and-figures-in-r-markdown-documents.] For example, the following prints a figure that takes up 75% of the page width. The plot also showcases the usage of the helper function `thl_colors()` which is included in the THLL \R\ package to easily retrieve the HEX code(s) of one or several of the colors specified in The Humane League's corporate design guidelines.

```{r fig-column, fig.align = 'center', out.width = "75%", fig.cap = "A plot generated with R.", echo=FALSE}
plot(runif(10), col = thll::thl_colors("red"))
```

While it possible to use standard Markdown syntax to include locally stored images, it's suggested to also use code chunks for these images. This makes it easy to format an image's appearence, add figure captions, and reference figures throughout the document (see below). The following figure is using a local image.

```{r fig-local, fig.align = 'center', out.width = "50%", out.extra='style="padding:20px"', fig.cap = "A local image.", echo=FALSE}
# find local image file in R package resources
thll_logo <- thll::find_resource("report-html", "images/thll-logo-stacked.png")

# print the figure
knitr::include_graphics(thll_logo)
```

## Cross-references

Using the Bookdown extension of R Markdown, it is possible to embed cross-references to different elements of the document.^[See https://bookdown.org/yihui/bookdown/cross-references.html.]

- `\@ref(section-id)` to reference sections, e.g., see Section \@ref(pandoc).
- `\@ref(fig:plot-name)` to reference figures, e.g., see Figure \@ref(fig:fig-local).
- `\@ref(eq:equation-name)` to reference equations, e.g., see Equation \@ref(eq:regression).

# HTML-specific features

## Tabs {.tabset}

Subsections (or lower level sections) can be displayed as tabs by adding the class attribute `.tabset` to the respectively higher sectioning command. This can be useful to present related material in a more compact way:

<p style="text-align: center;">**_OSF badges for open science practices_**</p>

### data

```{r fig-data-badge, fig.align = 'center', out.width = "35%", out.extra='style="padding:20px"', fig.cap = "OSF data badge.", echo=FALSE}
image <- thll::find_resource("report-html", "images/osf-data-large.png")
knitr::include_graphics(image)
```

### materials

```{r fig-materials-badge, fig.align = 'center', out.width = "35%", out.extra='style="padding:20px"', fig.cap = "OSF materials badge.", echo=FALSE}
image <- thll::find_resource("report-html", "images/osf-materials-large.png")
knitr::include_graphics(image)
```

### preregisteredbadge

```{r fig-preregistered-badge, fig.align = 'center', out.width = "35%", out.extra='style="padding:20px"', fig.cap = "OSF preregistered badge.", echo=FALSE}
image <- thll::find_resource("report-html", "images/osf-preregistered-large.png")
knitr::include_graphics(image)
```

### preregistered+

```{r fig-preregisteredplus-badge, fig.align = 'center', out.width = "35%", out.extra='style="padding:20px"', fig.cap = "OSF preregistered+ badge.", echo=FALSE}
image <- thll::find_resource("report-html", "images/osf-preregisteredplus-large.png")
knitr::include_graphics(image)
```

## Code-folding

By adding the YAML option `code_folding: hide` to the `output:` definition in the YAML header, one can automatically hide all code chunks.^[See https://bookdown.org/yihui/rmarkdown/html-document.html#code-folding] This is especially useful if one uses the HTML document as an analysis notebook, i.e., adds code chunks that contain many lines of code. Using the option makes the HTML document easier to digest while giving the reader the option to expand a given code chunk when they want to. Setting the option `code_folding: show` has the reverse effect, i.e., all code chunks are expanded by default but a reader can choose to close them. When not adding the `code_folding:` option (as demonstrated in this document), the reader does not have control over the display of code chunks.

# Random tips and tricks

- To render the same `.Rmd` to both PDF and HTML, but use partly different text/elements,
  - use the `alt()` function that is included in the THLL R package
  - for example, **this is `r thll::alt("\\LaTeX\\", "HTML")` output**

# Summary

This document gave a brief introduction on how to use the THLL R package and Pandoc/R Markdown syntax to generate THLL reports in HTML format. For more thorough information on how to use R Markdown, see the following web resources:

- [R Markdown guide](https://bookdown.org/yihui/rmarkdown/)
- [R Markdown cookbook](https://bookdown.org/yihui/rmarkdown-cookbook/)
- [R Markdown for scientists](https://rmd4sci.njtierney.com/)
- [Bookdown book](https://bookdown.org/yihui/bookdown/)

# Conflicts of interest

The Humane League Labs (THLL) performs scientific research to inform animal advocacy strategy. THLL is a program of, and currently fully funded through, The Humane League (THL), a 501(c)(3) nonprofit organization that "exists to end the abuse of animals raised for food." THLL is editorially independent from THL, and any other potential funders, in reporting research results. The design, execution, analysis, interpretation, and reporting of THLL research is performed entirely by THLL staff, without oversight by other THL staff or leadership. To further mitigate potential conflicts of interest, THLL demonstrates commitment to transparency by adhering to open science practices, including public preregistration of studies and analysis plans as well as publication of supporting data, computer code, and materials for all THLL research.

# References
